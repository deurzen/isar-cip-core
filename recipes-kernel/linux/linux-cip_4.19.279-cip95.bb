#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.19.y-cip"

SRC_URI[sha256sum] = "fb5808626317e464408203562941d287fc49f747f3af1c1a34c125229a5efa43"
